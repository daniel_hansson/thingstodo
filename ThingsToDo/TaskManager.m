//
//  TaskManager.m
//  ThingsToDo
//
//  Created by Daniel Hansson on 2016-02-10.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "TaskManager.h"

@interface TaskManager()
@property (nonatomic) NSMutableArray *tasks;
@property (nonatomic) NSUserDefaults *savedTasks;

@end

@implementation TaskManager

-(instancetype)init{
    self = [super init];
    if (self) {
        self.savedTasks = [NSUserDefaults standardUserDefaults];
        
        self.tasks = [[NSMutableArray alloc]init];
        
        
        [self loadTasks];
    }
    return self;
}

-(void)loadTasks{
    NSArray *temp = [self.savedTasks objectForKey:@"taskSavedData"];
    
    for(NSArray *task in temp) {
        NSMutableArray *newTask = [NSMutableArray arrayWithObjects:task[0], task[1], task[2], task[3], nil];
        [self.tasks addObject:newTask];
    }
    
}

-(void)saveTasks{
    NSUserDefaults *savedTasks = [NSUserDefaults standardUserDefaults];
    [savedTasks setObject:self.tasks forKey:@"taskSavedData"];
    [savedTasks synchronize];
}

-(void)removeTask:(NSUInteger)position{
    [self.tasks removeObjectAtIndex:position];
    [self saveTasks];
}


-(void)addTask:(NSString*)task withDescription:(NSString*)description isUrgent:(BOOL) urgent{
    NSLog(urgent ? @"Yes" : @"No");
    NSString *isUrgent;
    if (urgent) {
        isUrgent = @"urgent";
    }else{
        isUrgent = @"notUrgent";
    }
    
    NSMutableArray *newTask = [NSMutableArray arrayWithObjects:task, description, @"no", isUrgent, nil];
    NSLog([newTask description]);
    [self.tasks addObject:newTask];
    [self saveTasks];
}

-(void)doTask:(NSMutableArray*)task{
    NSLog([task description]);
    [task insertObject:@"yes" atIndex:2];
    [task removeObjectAtIndex:3];
    NSLog([task description]);
    [self saveTasks];
}

-(NSArray*)getTaskList{

    return self.tasks;
}

@end





