//
//  AddViewController.m
//  ThingsToDo
//
//  Created by Daniel Hansson on 2016-02-10.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "AddViewController.h"

@interface AddViewController ()
@property (weak, nonatomic) IBOutlet UITextField *TaskName;
@property (weak, nonatomic) IBOutlet UITextField *taskDescription;
@property (nonatomic, assign, getter=isUrgent) BOOL urgent;
@property (weak, nonatomic) IBOutlet UISwitch *urgentSwitch;

@end

@implementation AddViewController

- (IBAction)urgentChanged:(id)sender {
    //self.urgent = sender;
    //NSLog(self.urgent ? @"Yes" :@"No");
}

- (IBAction)addTask:(id)sender {
    if ([self.TaskName.text  isEqual: @""]) {
        self.TaskName.placeholder = @"YOU MUST SET A TITLE FOR THE TASK.";
    }else{
    
        if (self.urgentSwitch.on) {
            self.urgent = YES;
        }else {
            self.urgent = NO;
        }
        [self.taskManager addTask:self.TaskName.text withDescription:self.taskDescription.text isUrgent: [self isUrgent]];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(self.urgent ? @"Yes" :@"No");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
