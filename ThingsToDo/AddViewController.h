//
//  AddViewController.h
//  ThingsToDo
//
//  Created by Daniel Hansson on 2016-02-10.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TaskManager.h"

@interface AddViewController : UIViewController

@property (nonatomic) TaskManager *taskManager;

@end
