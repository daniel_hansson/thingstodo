//
//  DetailViewController.m
//  ThingsToDo
//
//  Created by Daniel Hansson on 2016-02-10.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *doTheTaskBtn;

@end

@implementation DetailViewController

- (IBAction)doTheTask:(id)sender {
    NSLog(@"doTheTaskBtn clicked");
    [self.taskManager doTask:self.task];
    self.doTheTaskBtn.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.task[0];
    self.descriptionLabel.text = self.task[1];
    
    if ([self.task[2] isEqualToString:@"yes"]) {
        self.doTheTaskBtn.hidden = YES;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
