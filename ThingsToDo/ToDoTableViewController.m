//
//  ToDoTableViewController.m
//  ThingsToDo
//
//  Created by Daniel Hansson on 2016-02-10.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "ToDoTableViewController.h"
#import "TaskManager.h"
#import "AddViewController.h"
#import "DetailViewController.h"

@interface ToDoTableViewController ()
@property (strong, nonatomic) TaskManager *taskManager;

@end

@implementation ToDoTableViewController


-(TaskManager*)taskManager{
    if (!_taskManager) {
        _taskManager = [[TaskManager alloc] init];
    }
    return _taskManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Things To Do";
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
     self.navigationItem.leftBarButtonItem = self.editButtonItem;
}

-(void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.taskManager getTaskList].count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"set cell color");
    UITableViewCell *cell;
    if ([[[[self.taskManager getTaskList]objectAtIndex:indexPath.row] objectAtIndex:2]  isEqual: @"yes"]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"DoneCell" forIndexPath:indexPath];
        
        cell.textLabel.text = [[[self.taskManager getTaskList]objectAtIndex:indexPath.row] objectAtIndex:0];
        
    }else if ([[[[self.taskManager getTaskList]objectAtIndex:indexPath.row] objectAtIndex:3]  isEqual: @"urgent"]){
        cell = [tableView dequeueReusableCellWithIdentifier:@"UrgentCell" forIndexPath:indexPath];
        
        cell.textLabel.text = [[[self.taskManager getTaskList]objectAtIndex:indexPath.row] objectAtIndex:0];
        
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        
        cell.textLabel.text = [[[self.taskManager getTaskList]objectAtIndex:indexPath.row] objectAtIndex:0];
    }
    
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self.taskManager removeTask:indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"TaskDetail"]) {
        
        DetailViewController *detailViewController = [segue destinationViewController];
        detailViewController.taskManager = self.taskManager;
        
        UITableViewCell *cell = sender;
        int row = (int)[self.tableView indexPathForCell:cell].row;
        detailViewController.task = [self.taskManager getTaskList][row];
        
    }else if ([segue.identifier isEqualToString:@"TaskAdd"]){
        
        AddViewController *addViewController = [segue destinationViewController];
        addViewController.taskManager = self.taskManager;
        
    }else if ([segue.identifier isEqualToString:@"DoneTaskDetail"]){
        DetailViewController *detailViewController = [segue destinationViewController];
        UITableViewCell *cell = sender;
        int row = (int)[self.tableView indexPathForCell:cell].row;
        detailViewController.task = [self.taskManager getTaskList][row];
    }
    
}


@end










