//
//  TaskManager.h
//  ThingsToDo
//
//  Created by Daniel Hansson on 2016-02-10.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TaskManager : NSObject

-(NSArray*)getTaskList;

-(void)addTask:(NSString*)task withDescription:(NSString*)description isUrgent:(BOOL) urgent;

-(void)removeTask:(NSUInteger)position;

-(void)saveTasks;

-(void)doTask:(NSMutableArray*)task;

@end
